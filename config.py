import logging

# This is a minimal configuration to get you started with the Text mode.
# If you want to connect Errbot to chat services, checkout
# the options in the more complete config-template.py from here:
# https://raw.githubusercontent.com/errbotio/errbot/master/errbot/config-template.py
import os

BACKEND = 'Slack'  # Errbot will start in text mode (console only mode) and will answer commands from there.

BOT_DATA_DIR = '/Users/shawn/code/src/gitlab.com/skift/botter/data'
BOT_EXTRA_PLUGIN_DIR = '/Users/shawn/code/src/gitlab.com/skift/botter/plugins'

BOT_LOG_FILE = '/Users/shawn/code/src/gitlab.com/skift/botter/errbot.log'
BOT_LOG_LEVEL = logging.DEBUG

BOT_ADMINS = ('@skift',)  # !! Don't leave that to "CHANGE ME" if you connect your errbot to a chat system !!

# Should plugin dependencies be installed automatically? If this is true
# then Err will use pip to install any missing dependencies automatically.
#
# If you have installed Err in a virtualenv, this will run the equivalent
# of `pip install -r requirements.txt`.
# If no virtualenv is detected, the equivalent of `pip install --user -r
# requirements.txt` is used to ensure the package(s) is/are only installed for
# the user running Err.
AUTOINSTALL_DEPS = True

BOT_IDENTITY = {
    'token': 'xoxb-50981114532-d5Or7PMNxVy3mp9BVv4jsT0V'
}

BOT_PREFIX = '!'
BOT_ALT_PREFIXES = ('<@U1GUV3CFN>', 'botter')
BOT_ALT_PREFIX_SEPARATORS = (':', ',', ';')
BOT_ALT_PREFIX_CASEINSENSITIVE = False

CHATROOM_PRESENCE = ()

##########################################################################
# Access controls and message diversion                                  #
##########################################################################

# Access controls, allowing commands to be restricted to specific users/rooms.
# Available filters (you can omit a filter or set it to None to disable it):
#   allowusers: Allow command from these users only
#   denyusers: Deny command from these users
#   allowrooms: Allow command only in these rooms (and direct messages)
#   denyrooms: Deny command in these rooms
#   allowprivate: Allow command from direct messages to the bot
#   allowmuc: Allow command inside rooms
# Rules listed in ACCESS_CONTROLS_DEFAULT are applied by default and merged
# with any commands found in ACCESS_CONTROLS.
#
# The options allowusers, denyusers, allowrooms and denyrooms support
# unix-style globbing similar to BOT_ADMINS.
#
# Command names also support unix-style globs and can optionally be restricted
# to a specific plugin by prefixing the command with the name of a plugin,
# separated by a colon. For example, `Health:status` will match the `!status`
# command of the `Health` plugin and `Health:*` will match all commands defined
# by the `Health` plugin.
#
# Please note that the first command match found will be used so if you have
# overlapping patterns you must used an OrderedDict instead of a regular dict:
# https://docs.python.org/3.4/library/collections.html#collections.OrderedDict
#
# Example:
#
#ACCESS_CONTROLS_DEFAULT = {} # Allow everyone access by default
#ACCESS_CONTROLS = {'status': {'allowrooms': ('someroom@conference.localhost',)},
#                   'about': {'denyusers': ('*@evilhost',), 'allowrooms': ('room1@conference.localhost', 'room2@conference.localhost')},
#                   'uptime': {'allowusers': BOT_ADMINS},
#                   'help': {'allowmuc': False},
#                   'help': {'allowmuc': False},
#                   'ChatRoom:*': {'allowusers': BOT_ADMINS},
#                  }
ACCESS_CONTROLS_DEFAULT = {}
ACCESS_CONTROLS = {
    'echo': None,

    'Flows:*': {
        'allowusers': BOT_ADMINS
    },

    'Help:apropos': None,

    'Health:*': {
        'allowusers': BOT_ADMINS
    },

    'Plugins:*': {
        'allowusers': BOT_ADMINS
    },

    'Utils:render': {
        'allowusers': '*'
    },
    'Utils:*': {
        'allowusers': BOT_ADMINS
    },

    'ChatRoom:*': {
        'allowusers': BOT_ADMINS
    },
}
# Uncomment and set this to True to hide the restricted commands from
# the help output.
HIDE_RESTRICTED_COMMANDS = True

# Uncomment and set this to True to ignore commands from users that have no
# access for these instead of replying with error message.
HIDE_RESTRICTED_ACCESS = True

BOT_EXTRA_STORAGE_PLUGINS_DIR='/Users/shawn/code/src/gitlab.com/skift/botter/data/plugins'
STORAGE = 'Firebase'
FIREBASE_DATA_URL = os.getenv('FIREBASE_DATA_URL')
STORAGE_CONFIG = {
    'data_url': 'https://project-1854802785227858744.firebaseio.com/',
    'secret'  : '6d82oiuPS3B4E7FWsUqQHTX9aNwTALpBeuEgEYnO',
    'email'   : 'shawn@skift.io'
}
